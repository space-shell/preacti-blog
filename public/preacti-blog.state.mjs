import * as R from 'https://cdn.skypack.dev/ramda'
import * as Rx from 'https://cdn.skypack.dev/rxjs'
import * as RxOp from 'https://cdn.skypack.dev/rxjs/operators'

import BlogFiles from './blog-files.js'

export const ObservableState = new Rx.BehaviorSubject(  )

const hash_file = (  ) =>
  BlogFiles
    .find( file =>
      new RegExp( `${ R.tail( window.location.hash ) }.md` ).test( file )
    )

ObservableState.next([ 'blog-file', hash_file(  ) || null ])

Rx.fromEvent( window, 'hashchange' )
  .pipe( RxOp.map( _ => [ 'blog-file', hash_file(  ) || null ] )
       )
  .subscribe( ObservableState )
