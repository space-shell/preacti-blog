import * as R from 'https://cdn.skypack.dev/ramda'
import { h } from 'https://cdn.skypack.dev/preact'
import { useEffect, useState } from 'https://cdn.skypack.dev/preact/hooks'
import htm from 'https://cdn.skypack.dev/htm'

const html = htm.bind( h )

const blog_file_name = 
  R.pipe
    ( R.split( '/' )
    , R.last
    , R.split( '.' )
    , R.head
    )

export const BlogTile = ({ title, on_hover }) => {
  const
    { length: l
    , [0]: st
    , [1]: nd
    , [2]: rd
    , [l - 3]: rdls
    , [l - 2]: ndls
    , [l - 1]: ls
    }
  = title

  return html`
    <blog-tile>
      <a
        href=${ `#${ title }` }
        onMouseLeave=${ (  ) => on_hover( null ) }
        onMouseEnter=${ (  ) => on_hover( title ) } >

        <span>${ st }<//>

        <span>${ nd }<//>

        <span>${ rd }<//>

        <span> · <//>

        <span> · <//>

        <span> · <//>

        <span>${ rdls }<//>

        <span>${ ndls }<//>

        <span>${ ls }<//>
      <//>
    <//>
  `
}

export const BlogTiles = ({ blogs, on_hover }) =>
  R.isNil( blogs )
    ? html`
        <${ BlogTile } name=··· />
      `
    : html`
        <blog-tiles>
          ${blogs.map( blog => html`
              <${ BlogTile }
                title=${ blog_file_name( blog ) }
                on_hover=${ on_hover } /> ` ) }
        <//> `
