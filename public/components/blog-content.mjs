import * as R from 'https://cdn.skypack.dev/ramda'
import * as Rx from 'https://cdn.skypack.dev/rxjs'
import * as RxOp from 'https://cdn.skypack.dev/rxjs/operators'
import * as RxFetch from 'https://cdn.skypack.dev/rxjs/fetch'
import { h, render } from 'https://cdn.skypack.dev/preact'
import { useEffect, useState } from 'https://cdn.skypack.dev/preact/hooks'
import { Remarkable } from 'https://cdn.skypack.dev/remarkable'
import htm from 'https://cdn.skypack.dev/htm'

import { useObservable } from '../hooks/use-observable.mjs'

const html = htm.bind( h )

const markdown = new Remarkable(  )

const proc_blog_content = obsv =>
  obsv.pipe
    ( RxOp.filter
        ( R.o( R.equals( 'blog-file' ) , R.head ) )
    , RxOp.map( R.last )
    , RxOp.switchMap
        ( url => RxFetch
            .fromFetch( url )
            .pipe
              ( RxOp.switchMap( response =>
                  response.status > 400
                    ? [ , null ] // Really forgot why this needs to be a tuple
                    : response.text(  ) ) )
              )
    , RxOp.catchError( console.log )
    )

export const BlogContent = ({ obsv, children }) => {

  const blog_content = useObservable( proc_blog_content( obsv ) )

  const [ markup , set_markup ] = useState(true)

  if ( R.isNil( blog_content ) )
    return html`
      <blog-content>
        ${ children }
      <//> `

  return html`
    <blog-content>
      <button
        type='button'
        onClick=${ (  ) => set_markup( !markup ) }>
        
        ${ markup ? 'HTML' : 'MD' }
      <//>

      <hr />

      ${ markup
          ? html`
              <div
                className='markdown'
                dangerouslySetInnerHTML=${{ __html: markdown.render( blog_content )} } />
            `
          : html`
              <div className='markup'>

                ${ blog_content }
              <//>
            `
      }
    <//>
  `
}

