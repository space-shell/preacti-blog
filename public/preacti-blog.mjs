// import 'https://cdn.skypack.dev/preact/devtools'
import * as R from "https://cdn.skypack.dev/ramda";
import * as RxOp from "https://cdn.skypack.dev/rxjs/operators";
import { Fragment, h, render } from "https://cdn.skypack.dev/preact";
import { useState } from "https://cdn.skypack.dev/preact/hooks";
import htm from "https://cdn.skypack.dev/htm";

import BlogFiles from "./blog-files.js";
import { ObservableState } from "./preacti-blog.state.mjs";
import { useObservable } from "./hooks/use-observable.mjs";
import { BlogTiles } from "./components/blog-tiles.mjs";
import { BlogContent } from "./components/blog-content.mjs";

const html = htm.bind(h);

const proc_blot_title = (obsv) =>
  obsv.pipe(RxOp.filter(R.o(R.equals("blog-title"), R.head)), RxOp.map(R.last));

const PreactiBlog = ({ obsv }) => {
  const state = useObservable(obsv);

  const blog_title = useObservable(proc_blot_title(obsv));

  return html`
    <header>
      ${blog_title || "Space-Shell"}
    <//>

    <loop-write data-speed=50 data-word=blog />

    <${BlogTiles}
      blogs=${BlogFiles}
      on_hover=${(x) => ObservableState.next(["blog-title", x])} />

    <${BlogContent}
      obsv=${ObservableState} >

      <header>
        Welcome
      <//>
    <//>

    <footer>
      <a href='https://github.com/space-shell'> Github <//>

      <a href='https://j-n.me.uk'> Website <//>

      <a href='https://cv.j-n.me.uk'> C V <//>

      <a href='mailto:contact@j-n.me.uk'> Email <//>
    <//> `;
};

window.customElements.define(
  "preacti-blog",
  class extends HTMLElement {
    constructor() {
      super();
    }

    connectedCallback() {
      render(
        html`
              <${PreactiBlog}
                obsv=${ObservableState} />`,
        this,
      );
    }
  },
);
