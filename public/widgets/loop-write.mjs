const repeat_word =
  length =>
  word =>
    Array
      .from({ length })
      .fill( word )
      .join( ' | ' )

const template = ({ word }) => `
    <style>
      :host {
        white-space: nowrap;
        pointer-events: none;
        overflow: hidden;
      }

      :host > span {
        position: relative;
        display: inline-block;
      }

      :host > span:nth-of-type(1) {
      }

      :host > span:nth-of-type(2) {
      }
    </style>

    <span>
      ${ repeat_word( 10 )(word) }
    </span>

    <span>
      ${ repeat_word( 10 )(word) }
    </span>
  `

export const LoopWrite = class extends HTMLElement {
  constructor(...args) {
    super(...args);

    this.$ = {  }

    this.$.shadow = this.attachShadow({ mode: "closed" })
  }

  get velocity(  ) {
    if (!this.$.speed) return this.$.duration;

    return Math.round(
      (this.$.shadow.lastElementChild.offsetWidth / this.$.speed) * 1000
    );
  }

  connectedCallback(  ) {

    const
      { duration
      , speed
      , word
      } = this.dataset

    this.$ =
      { ...this.$
      , duration: parseInt( duration || 2500 )
      , speed: parseInt( speed || 0 )
      , word
      }

    this.$.shadow.innerHTML =
      template({ word: this.dataset.word })

    const awaitLoop = retry => {
      if (!retry || !this.$.shadow.lastElementChild.offsetWidth)
        return window.requestAnimationFrame(() => awaitLoop(retry - 1));

      this.$.shadow.querySelectorAll("span").forEach((span, idx) => {
        span.animate(
          [{ transform: `translateX(0)` }, { transform: `translateX(-100%)` }],
          { duration: parseInt(this.velocity)
          , iterations: Infinity
          }
        );
      });
    };

    if (this.$.speed) awaitLoop(10);
    else awaitLoop(0);
  }
};
