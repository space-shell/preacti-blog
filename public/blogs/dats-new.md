# Dat's New

19-02-2021

## The Internet's structure is ever evolving

---

The internet is ours! This has never been more true than it is right now, centralised server architectures are breaking apart, microservices are spreading like wildfire and p2p communications are at the heart of the futures economy.

---

### The Social Explosion

I wasn't around when the first few messages were being sent across the CERN laboratory and I just about remember the dial-tone of my 52k modem, my internet began with a single page on site call MySpace.com. MySpace and similar social networking sites such as Angelfire allowed people to customize a single static page with whatever content they liked using basic CMS tools and simple markup. Just like the rest of the early 2000's this gave rise to a mish-mash of personal pages with a wide range of unfiltered personal content, lawless and free until Facebook stepped in to take a little control of the situation.

Facebook's governance gave simplicity, uniformity and safety to the users, sleek simple profiles could be made up in minutes and with explicit content being moderated it provided a safe enough environment unlike it's contrary The Onion Relay ( TOR ). TOR differed to facebook in everyway possible except for the fact that users could just as simply create personal spaces that could be shared with the world. TOR is not a centralised website like Facebook, it is infact a decentralised protocol that allows any user to generate a unique URL that can share content with anyone across the TOR network. Where both Facebook and TOR's ideologies began with good intent, fast forward to today and Facebook has become a `Big Brother` like environment where private details of platform users are sold off for digital processing and the TOR platform has deteriorated to cesspool of some of the worst internet content publicly available. In comes the Dat protocol ( and many others ).


### Working with Dat's

**Browsing Dat's**

In order to browse sites hosted using the Dat protocol we need a separate browser that understands how they work. The Beaker Browser is a fork of the open source Chromium browser with the added functionality of viewing, generating and forking Dat websites.

[ Beaker Browser ]( https://beakerbrowser.com )

**Generating Dat's**

Here's the nitty gritty sharing your existing website on the Dat protocol, first we need to set up our Node environment. Using your favourite package manager you will have to install the Node.

> [ Installing Node ]( https://nodejs.org/en/download/package-manager/ ) - Using a package manager to install node

```bash
brew install nodejs
```

With Node installed we require the `dat` package and optionally the `dat-store` package if you are looking to keep your site live permanently.

```bash
sudo npm i -g dat dat-store
```

With the required packages installed setting up a Dat site is simple, navigate to your publicly served folder ( any statically served files ) and run the `dat share` command.

```bash
dat share
```

This command will produce unique `dat://{ key }` url and display any p2p connections.

---

### References

* [ Dat Foundation ]( https://www.torproject.org )
* [ TOR ]( https://www.torproject.org )
* [ Facebook ]( https://www.facebook.com )
