# Build Less

23-03-2021, 19-02-2021

## The end of a build dependent era

---

The people have spoken and the web has heard, gone were the days of being bound to a Front-End build environment and this site here is living proof.

---

### The problems that build systems had addressed

At one point JavaScript as a language undertook massive expansion within a short period of time, this change continues to be a major landmark in the language’s development today and is known by its release version ES6. ES6 changed people's perception of the language as well as introducing and enhancing a number of programming paradigms, however, it is now 5 years old and to date there has been at least 20 major additions and enhancements to the ECMAScript standard. 

During the transition phase from ES5 to ES6 in 2015, there was a disparity between what JavaScript could do in theory and the way in which web browsers implemented all of the newest functionality that the language had to offer. This disconnect gave rise to Babel, a JavaScript transpiler that re-writes all of the latest features as older more compatible code, it allowed developers to utilize the languages full capabilities and ensured the output code would run in all browsers. 

It didn’t stop at the ES6 transition, the language continued to grow as more standards were set and features were added on a regular basis. Babel made sure to accommodate for all of the latest changes and more however once developers began to implement Babel into their workflows, there really was no going back. The source code developers were currently working with was no longer compatible across all browsers, and some experimental features that Babel had pre-emptively implemented had not managed to make it into the main language, this locked developers into the Babel ecosystem. Unless developers refactored out the Babel specific implementations from their source code and introduced the standardised language features the reality of JavaScript as an interpreted language began to slowly fade away. 

As JavaScript’s popularity began to grow exponentially, sharing code rapidly became the norm due to its uncompiled nature and the ease of integration, a HTML tag was all that was required to extend an applications functionality. Most JavaScript libraries would be hosted on their own servers or CDN’s until eventually the Node Package Management system (NPM) was introduced. 

Like most other package management systems, NPM standardized how developers would integrate external libraries as well as providing a single index in which to find, sort and filter said libraries. With a standard package management system in place it wasn’t long before there was a package available for just about anything ranging from packages that provided their own domain specific languages and custom transpilers that provide additional language functionality to packages consisting of as little as three lines of code. Preprocessing, transpilation and post processing became common practice when dealing with the web stack ( HTML, CSS and JS ) and Webpack rose to become the de-facto tool for processing and producing the large variety of files and file types that made up the ever evolving stack of web technologies, "Vanilla" JavaScript was a thing of the past.

---

### The build system alternatives that are available today

ES10 is here and with it a sense of direction and stability, the language is a lot more functional than it was before and comes along with a single uniform modules system. Significant changes to the JavaScript syntax have slowed and as of writing a majority of browsers include the entire language spec, having to depend Babel for cross platform compatibility is becoming a thing of the past but where Babel had tried so hard to be ahead of the trend it will always be a requirement for features that never made it into the language.

**JSX ⇒ HTM**

One of the noticeable features of Babel is the way it is used to transform JSX into JavaScript, JSX is not part of the language at all and most probably never will be, for this reason any codebase utilising JSX will require Babel by default. HTM to the rescue, ES6 came with template strings which considers structure within a string variable, HTM has leveraged this to create a library that compiles string templates into virtual DOM generation code and can therefore run React within the browser ( although for this case, Preact would be a wise alternative ).

```javascript
// JSX Example
const JSXButtonComponent = ({ onClick }) =>
  <div
    type='button'
    className='btn'
    onClick={ onClick } >

    Click
  </div>

// HTM Example using template strings
const HTMButtonComponent = ({ onClick }) =>
  html`
    <div
      type=button
      class=btn
      onClick=${ onClick } >

      Click
    <//>`
```

**NPM ⇒ Pika / Snowpack**

NPM like most good package managers retrieves and stores versioned code locally, due to the versatility of NPM this process generates a huge overhead of collecting sub-dependencies, TypeScript types and server side libraries. Bundle tools aggregate libraries from NPM and then optimise those bundles into smaller chunks but with ES modules providing the ability to import code from a remote source there is no need for a central package repository. Pika is a platform that provides ES module URL's for direct imports into your project, libraries are loaded dynamically upon request and cached between calls, packages are versioned and switching over to decentralised modules could not be simpler. Snowpack acts as a mediator that compiles individual NPM modules into your public directory for direct imports from the browser.

```javascript
// Module loaded from NPM for bundling
import * as Preact from 'preact'
```

```javascript
// Module loaded from the pika.dev CDN
import * as Preact from 'https://cdn.pika.dev/preact@^10.3.0'
```

```javascript
// Module loaded through snowpack
import * as Preact from '/web-modules/preact.js'
```

**Webpack ⇒ es-module-shims**

Bundling and transpilling are only a few steps in the Front-End development build process, these days projects leverage a number of files and formats which is another benefit to using a bundle system. The `es-module-shims` library provides a polyfill / patch to extend the ES module functionality for importing JSON files, CSS modules and event import maps to import modules without a relative / absolute path.

**SASS ⇒ LESS**

This is a simple one, LESS is a fully fledged CSS preprocessor, written in JavaScript and runs in the browser.

---

### The future of JavaScript and Front-End development

Going forwards I can for-see that some of the features introduced by the `es-module-shims` library will eventually make it into the JavaScript specification and that services like those offered by Pika.dev will become more predominant and in turn the language can slowly start to revert back to the scripted and interpreted nature it was born from.

---

### References

* [ Pika.dev ]( https://www.pika.dev )
* [ Snowpack ]( https://www.snowpack.dev )
* [ es-module-shims ]( https://github.com/guybedford/es-module-shims )
* [ LESS ]( http://lesscss.org )
* [ HTM ]( https://github.com/developit/htm )
* [ Babel ]( https://babeljs.io )
* [ Webpack ]( https://webpack.js.org )

