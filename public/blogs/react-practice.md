# React in Practice

19-02-2021

## A Guide Into the React mentality

---

A React project doesn't start within the editor, or the command line, it starts on the whiteboard.

---

### What? Why? How?

Created and managed by Facebook, React is an open-source JavaScript framework used primarily for DOM manipulation and state management. React is fast, efficient and clean in terms of architecture. One unique ( at the time ) selling point of the React library is its implementation of its component model and its ability to encapsulate segments of state.

The React framework was developed as more logic was being inject into the browser client and helped address the growing issue of the misuse of global state. As a scripting language the access to global variables played a big part in the rapid development cycle of front-end functionality allowing for simple sharing and debugging of information. jQuery was a popular front-end library that really leveraged the use of global state and quickly became the de-facto library for any front-end interactions but as developers began to stretch the capabilities of what could be achieved within the browser, scripts slowly started growing into application. Although jQuery specifically relied on the use of global variable it had no control over what would be updated and when, this led to a common pattern emerging where each step within any given application would follow the process of checking a DOM element, checking a desired condition and then calculating a DOM manipulation. This pattern would lead to the subsequent overuse of flags and eventually managing state would become a manual burden. 

DOM manipulation is slow, the majority of DOM manipulations trigger a chain of events within the browser until the page is rendered and the display is finally updated. With computer processing power increasing at an exponential rate, the React team developed an optimization technique that would help reduce the overall number of DOM updates to a minimum by recreating the DOM in memory. This recreation of the DOM structure is know as the VDOM ( Virtual Document Object Model ) and allowed applications to query, diff and update this new structure much faster than they could before. With this new found power the React library introduced a new programming paradigm to the front-end community, one where developers could focus on the state of the of the state and have the library, calculate and update the DOM automatically in the background.

Fast forward on to now, the research and development that has gone into the React project has given rise to event further optimization techniques and from v16 the introduction of `fiber`.

### Composition Breakdown

Before making a start on the creation of a React application, understanding where to draw the visual and technical boundaries is required.  This process involves breaking down a visual design, mentally into a hierarchy of React components and state diagrams.

A good first step into drawing the boundaries would be to identify any static elements and how those elements could be reused and modified to produce variations. An example of these static components would include elements such as buttons, images, cards and articles. These elements that have been initially identified could then be sorted into groups where components could share functionality and create variations depending on some given input information.

Secondly persistent elements would include items that would handle a single responsibility and would more than likely be themselves a composition of one or more static elements. Persistent elements should have a long lifespan throughout the user journey with a very small chance of being removed from the page at any given moment. An example of a persistent element would be navigation / breadcrumb bar or an aside section, although these component may update visually they're less likely to be loaded dynamically.

Finally the interactive elements should be left unidentified, grouping these elements is more of a technical task than planning the other groups since the main consideration for dividing interactive elements is how and when to share data between their corresponding components. For this task it's very important to understand that React manages the flow of data in only a single direction, from top to bottom.

### Data Structure Compilation

One question that requires considerable consideration when planning the data structure of a React application is weather or not state should be managed by React or a third party library, particularly Redux. React provides multiple design patterns to help manage, organise and share state between components. Smaller applications could benefit from keeping development within the React library, whereas larger applications could benefit from the extensibility and development tools that come with Redux.

React's data flow between components is strictly unidirectional where data is passed from the parent components to the children, keeping state within the component would require all components within the application to reside within a single parent and to have that parent store the entire application state and pass that state down. Having a single parent component is not necessarily an issue and is usually best practice, especially since the introduction of Reacts `Lazy` and `Suspense` components, but passing the entire state from the root downwards can lead to significant bloating within the upper components props where the upper components are accepting data for the sole purpose of passing it the their children. Having data passing throughout components is know as `prop-drilling` and can cause the source code to become unreadable at a larger scale.

In order to avoid the overuse of prop-drilling Reacts solution is the concept of context providers, React provides a `createContext` helper function that returns a component which, in turn, accepts state as property and children to pass that state onto. Context providers remove the necessity for prop drilling by providing the ability to pass the input state to any child component with the boundaries of its own sub-hierarchy. Although this provider pattern removes the prop-drilling issue by providing data to it's child component, it doesn't inherently come with a strategy for updating that data and propagating those updates throughout the application.

With potentially multiple providers and a variety of `setter` functions being stored within each providers state, the overall state of the application can quickly become unwieldy. Again the React library provides a solution to this very dilemma, through the use of Reducer functions. A state reducer usually consists of an immutable data structure that represents the applications state and a single update function that accepts a label and a value, upon each invocation the label matches a case statement which in turn executes a block of code that has access to both the state and the provided input value, finally the reducer function returns a newly updated state in it  entirety. By combining reducers with providers React provides a clean interface for accessing and modifying data between elements within a parent child ( PC ) hierarchy and through the use of multiple providers a way to have sibling component interact in a modular fashion.

The React library reaches it's limitations when it comes to asynchronicity, and in particular asynchronous updates via reducer functions and context providers, fortunately this is the point where Redux picks up. Redux provides the Provider and Reducer patterns as standard as well as the additional middleware paradigm which allows for further extensibility. Redux's extensibility allows the developer to merge the existing state management patterns with a multitude of others, each of which are best suited to some particular situation.

#### Redux's Extended State Management Paradigms

**Coroutine Pattern - redux-thunk**
Delays asynchronous operations, through the use of `thunks` in order to execute operations sequentially and in order.

**Observer Pattern - redux-observable**
Extends both React and Redux to incorporate the observer pattern, enabling event subscriptions and stream like functionality.

**Generator Pattern - redux-saga**
Through the use of JavaScript generator functions, asynchronous operations can be paused and cancelled synchronously.

### Component State Management

When planning out how to manage and compose state between React components it's usually best to start from the bottom of a parent child hierarchy and work upwards. For each component evaluate the following:

- What data is static
- What data is default ( optional )
- What data is dynamic ( required )
- The possible user interactions
- Any lifecycle actions, interactions
- Any possible error boundaries

For each step from child to the parent it's important to consider what data the parent owns and what data the parent component has access to either via grandparent components or external state interaction. By planning in steps from bottom to top each edge element of the hierarchy tree should only have access to the minimal amount of information required and the overall application should create and inverted triangle of data dependency. External state interactions reduces the necessity for the inverted child parent interactions, and prop drilling but in turn reduces the modularity of components throughout the necessity of external dependencies. In short, pure-ish components that accept all required data as input and pass on information to their children are readily reusable and easily extensible.

### Start Coding

With the initial planning portion of the application development now complete all that is left is the implementation, testing, refactoring, and analysis. Following the plan as best as possible and sticking to the application boundaries should help the rest of the development process fall into place, tests can be devised around the application boundaries and refactoring work should still adhere to the initial plan.

---

### References

* [React]( 'https://reactjs.org/docs/getting-started.html' )
* [Redux]( 'https://redux.js.org/' )
* [Redux Thunk]( 'https://github.com/reduxjs/redux-thunk' )
* [Redux Observable]( 'https://redux-observable.js.org/' )
* [Redux Saga]( 'https://redux-saga.js.org/docs/introduction/' )
