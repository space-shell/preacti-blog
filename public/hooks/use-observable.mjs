import * as RxOp from 'https://cdn.skypack.dev/rxjs'

import { useState, useEffect, useRef } from 'https://cdn.skypack.dev/preact/hooks'

export const useObservable = observable =>
  {
    // TOOD - JN - Refactor to apply internal observable state and return an update method
    // const observable_state = useRef( new Rx.Subject )

    const [ state, set_state ] = useState( null )

    useEffect( (  ) => {
      observable.subscribe( set_state )
      
      return (  ) => observable.unsubscribe(  )
    }, [  ] )

    return state
  }
