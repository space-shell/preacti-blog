# Preacti Blog

[ Pages ]( https://space-shell.gitlab.io/preacti-blog )
[ Live ]( blogs.j-n.me.uk )

This is a my blog site, the site does not require any build process and can be hosted statically.

## Action Commands

The file `run` can be executed to perform various commands such as deployment and dev serving.

## Writing Blogs

Blog files are written in markdown and stored within the `public/blogs` folder. The blogs to display on the website should be added to the list within the `blog-files.js` file.
